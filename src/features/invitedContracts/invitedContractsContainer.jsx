import { connect } from 'react-redux';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { fetchInvitedContracts } from '../../actions/contractsActions';

class InvitedContractsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = { fetchingContracts: false };
  }

  handleContractsClick(roleId) {
    const { fetchContractsList } = this.props;
    this.setState({ fetchingContracts: true });
    fetchContractsList(roleId);
  }

  render() {
    const { roleId, invitedContractsList, fetchedOnceAlready } = this.props;
    const { fetchingContracts } = this.state;

    if (fetchedOnceAlready && invitedContractsList.length === 0) {
      return <h2>No contracts</h2>;
    }

    if (fetchingContracts && !fetchedOnceAlready) {
      return <h2>Loading</h2>;
    }

    if (invitedContractsList.length === 0) {
      return <button onClick={() => this.handleContractsClick(roleId)}>Invited Candidates</button>;
    }

    return (
      <ul>
        {invitedContractsList.map(({ candidateName }) => (
          <li key={candidateName}>{candidateName}</li>
        ))}
      </ul>
    );
  }
}

InvitedContractsContainer.propTypes = {
  roleId: PropTypes.number.isRequired,
  invitedContractsList: PropTypes.arrayOf(
    PropTypes.shape({
      candidateName: PropTypes.string.isRequired,
    }),
  ),
  fetchContractsList: PropTypes.func.isRequired,
  fetchedOnceAlready: PropTypes.bool.isRequired,
};

InvitedContractsContainer.defaultProps = {
  invitedContractsList: [],
};

const mapStateToProps = (state, otherProps) => ({
  invitedContractsList: state.contracts.contractsLists[otherProps.roleId] || [],
  fetchedOnceAlready: state.contracts.contractsLists[otherProps.roleId] !== undefined,
});

const mapDispatchToProps = dispatch => ({
  fetchContractsList: roleId => dispatch(fetchInvitedContracts(roleId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(InvitedContractsContainer);

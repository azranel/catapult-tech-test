import PropTypes from 'prop-types';
import React from 'react';

const ShiftsFilter = ({ label, possibleValues, selectedValue, onFilterChange }) => (
  <div>
    <h3>{label}</h3>
    <select
      value={selectedValue}
      onChange={event => {
        onFilterChange(event.target.value);
      }}
    >
      <option value="_all">All</option>
      {possibleValues.map(value => (
        <option key={value} value={value}>
          {value}
        </option>
      ))}
    </select>
  </div>
);

ShiftsFilter.propTypes = {
  label: PropTypes.string.isRequired,
  possibleValues: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedValue: PropTypes.string,
  onFilterChange: PropTypes.func.isRequired,
};

ShiftsFilter.defaultProps = {
  selectedValue: '_all',
};

export default ShiftsFilter;

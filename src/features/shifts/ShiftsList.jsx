import PropTypes from 'prop-types';
import React, { useState } from 'react';
import InvitedContractsContainer from '../InvitedContracts/InvitedContractsContainer';
import ShiftsFilter from './ShiftsFilter';

const ShiftsList = ({ list }) => {
  const jobTypes = [...new Set(list.map(shift => shift.jobType))];

  const [visibleJobType, onJobTypeSelection] = useState('_all');
  const [selectedDayPart, onDayPartSelection] = useState('_all');

  let visibleShifts =
    visibleJobType === '_all' ? list : list.filter(({ jobType }) => jobType === visibleJobType);

  if (selectedDayPart !== '_all') {
    if (selectedDayPart === 'AM') {
      visibleShifts = visibleShifts.filter(({ startTime }) => Number(startTime.slice(0, 2)) < 12);
    } else {
      visibleShifts = visibleShifts.filter(({ startTime }) => Number(startTime.slice(0, 2)) > 12);
    }
  }

  return (
    <div>
      <h1>Shifts List</h1>

      <h2>Shifts filter</h2>

      <ShiftsFilter
        label="Job types"
        possibleValues={jobTypes}
        selectedValue={visibleJobType}
        onFilterChange={onJobTypeSelection}
      />
      <ShiftsFilter
        label="Start time part of the day"
        possibleValues={['AM', 'PM']}
        selectedValue={selectedDayPart}
        onFilterChange={onDayPartSelection}
      />

      <hr />

      {visibleShifts.map(shift => (
        <div data-test-shift key={shift.roleId}>
          Job type: {shift.jobType} <br />
          Start date: {shift.shiftDate} <br />
          Start time: {shift.startTime} <br />
          End time: {shift.endTime} <br />
          <InvitedContractsContainer roleId={shift.roleId} />
          <hr />
        </div>
      ))}
    </div>
  );
};

ShiftsList.propTypes = {
  list: PropTypes.arrayOf(
    PropTypes.shape({
      roleId: PropTypes.number.isRequired,
      shiftDate: PropTypes.string.isRequired,
      startTime: PropTypes.string.isRequired,
      endTime: PropTypes.string.isRequired,
      staff_required: PropTypes.number.isRequired,
      number_of_invited_staff: PropTypes.number.isRequired,
      jobType: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

export default ShiftsList;

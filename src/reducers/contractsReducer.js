const InitialState = {
  contractsLists: {},
  fetchingContracts: false,
}

const contractsReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'FETCH_INVITED_CONTRACTS_LIST_FULFILLED': {
      const {
        payload: {
          config: {
            params: {
              roleId
            }
          },
          data
        }
      } = action
      const roleContractsList = {}
      roleContractsList[roleId] = data
      const newContractsLists = Object.assign({}, state.contractsLists, roleContractsList);
      return Object.assign({}, state, {
        contractsLists: newContractsLists,
      })
    }

    default:
      return state
  }
}

export default contractsReducer

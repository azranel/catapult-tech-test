import { combineReducers } from 'redux';
import createStore from './createStore';
import shiftsReducer from './shiftsReducer';
import contractsReducer from './contractsReducer';

const catapultReducer = combineReducers({
  shifts: shiftsReducer,
  contracts: contractsReducer,
});

export default createStore(catapultReducer);

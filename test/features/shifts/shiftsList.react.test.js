import ShiftsList from '../../../src/features/shifts/ShiftsList';
import React from 'react';
import {
  shallow,
  mount
} from 'enzyme';

const jobList = [{
    roleId: 1,
    shiftDate: '2020-02-29',
    startTime: '10:00',
    endTime: '17:00',
    staff_required: 5,
    number_of_invited_staff: 0,
    jobType: 'Security',
  },
  {
    roleId: 2,
    shiftDate: '2020-03-01',
    startTime: '10:00',
    endTime: '14:00',
    staff_required: 2,
    number_of_invited_staff: 0,
    jobType: 'Receptionist',
  },
];

describe('ShiftsList', () => {
  it('renders shifts', () => {
    const component = shallow( <
      ShiftsList list = {
        jobList
      }
      />
    );

    expect(component).toMatchSnapshot();
    expect(component.find('[data-test-shift]').length).toEqual(jobList.length);
  });
});

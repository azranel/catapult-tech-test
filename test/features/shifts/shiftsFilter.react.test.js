import ShiftsFilter from '../../../src/features/shifts/ShiftsFilter';
import React from 'react';
import { mount } from 'enzyme';

const label = 'Job Types';
const possibleValues = ['Receptionist', 'Factory helper', 'Shop staffer'];
const selectedValue = '_all';
const onSelect = jest.fn();

describe('ShiftsFilter', () => {
  it('renders also "all" field', () => {
    const component = mount(
      <ShiftsFilter
        label={label}
        possibleValues={possibleValues}
        selectedValue={selectedValue}
        onFilterChange={onSelect}
      />,
    );

    expect(component).toMatchSnapshot();
    expect(component.find('option').length).toEqual(possibleValues.length + 1);
  });

  it('triggers passed down callback whenever value changes', () => {
    const component = mount(
      <ShiftsFilter
        label={label}
        possibleValues={possibleValues}
        selectedValue={selectedValue}
        onFilterChange={onSelect}
      />,
    );

    expect(component).toMatchSnapshot();

    const event = { target: { value: possibleValues[0] } };
    component.find('select').simulate('change', event);

    expect(onSelect).toBeCalledWith(possibleValues[0]);
  });
});
